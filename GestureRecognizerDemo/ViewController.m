//
//  ViewController.m
//  GestureRecognizerDemo
//
//  Created by James Cash on 16-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "ColorModel.h"

@interface ViewController ()

@property (nonatomic,strong) ColorModel *model;
@property (weak, nonatomic) IBOutlet UIView *displayView;
@property (nonatomic, strong) UILongPressGestureRecognizer *longPressRecognizer;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.model = [[ColorModel alloc] init];

    self.longPressRecognizer = [[UILongPressGestureRecognizer alloc]
                                initWithTarget:self
                                action:@selector(longPressOnView:)];
    [self.displayView addGestureRecognizer:self.longPressRecognizer];
    
    // Don't need to do this in *this* case, but if it were a UIImageView, we would need this or gesture recognizers wouldn't fire
    self.displayView.userInteractionEnabled = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateDisplayView
{
    self.displayView.backgroundColor = self.model.color;
    CGRect updatedFrame = self.displayView.frame;
    updatedFrame.size.width *= self.model.scale;
    updatedFrame.size.height *= self.model.scale;
    self.displayView.frame = updatedFrame;
}

- (IBAction)panOnView:(UIPanGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [sender translationInView:self.view];
        CGPoint scaledPoint = CGPointMake(                                        translation.x / CGRectGetWidth(self.displayView.frame),
                                          translation.y / CGRectGetHeight(self.displayView.frame));
        [self.model changeColorWithComponents:scaledPoint];
        [self updateDisplayView];

        CGPoint velocity = [sender velocityInView:self.view];
        CGFloat speed = sqrt(velocity.x * velocity.x + velocity.y * velocity.y);
        NSLog(@"Translation was moving at %f points/sec", speed);

        // Resetting the translation to zero, so this method only sees incremental changes, not the total
        [sender setTranslation:CGPointZero inView:self.view];
    }
}

- (IBAction)pinchedOnView:(UIPinchGestureRecognizer *)sender {
    self.model.scale = sender.scale;
    [self updateDisplayView];
    [sender setScale:1];
}

- (void)longPressOnView:(UILongPressGestureRecognizer*)sender {
    CGPoint pressLocation = [sender locationInView:self.view];
    self.displayView.center = pressLocation;
}

// z^2 = x^2 + y^2
// speed = sqrt(x*x + y*y)

@end
