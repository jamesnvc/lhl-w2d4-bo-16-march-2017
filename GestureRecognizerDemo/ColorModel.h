//
//  ColorModel.h
//  GestureRecognizerDemo
//
//  Created by James Cash on 16-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface ColorModel : NSObject

@property (nonatomic,strong) UIColor *color;
@property (nonatomic,assign) CGFloat scale;

- (void)changeColorWithComponents:(CGPoint)redGreen;

@end
