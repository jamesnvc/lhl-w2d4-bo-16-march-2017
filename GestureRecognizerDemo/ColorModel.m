//
//  ColorModel.m
//  GestureRecognizerDemo
//
//  Created by James Cash on 16-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ColorModel.h"

@implementation ColorModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        _color = [UIColor redColor];
        _scale = 1.0;
    }
    return self;
}

- (void)changeColorWithComponents:(CGPoint)redGreen
{
    CGFloat oldRed, oldGreen, oldBlue, oldAlpha;

    [self.color getRed:&oldRed green:&oldGreen blue:&oldBlue alpha:&oldAlpha];

    UIColor *newColor = [UIColor colorWithRed:oldRed + redGreen.y
                                        green:oldGreen + redGreen.x
                                         blue:oldBlue
                                        alpha:oldAlpha];
    self.color = newColor;
}

@end
